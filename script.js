
let trainer ={

    name: "Ash Ketchum",
    age: "10",
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
        Hoenn: ["May","Max"],
        Kanto: ["Brock", "Misty"]
    },
    talk: function(){
        console.log(`Pikachu! I choose you`);
    }
};

console.log(trainer);
console.log(trainer.name);
console.log(trainer["pokemon"]);
trainer.talk();

function Pokemon(name,level){

    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level;

    this.tackle = function(target){
        console.log(`${this.name} tackled ${target.name}`);
        console.log(`${target.name}'s health is now reduced to ${target.health - this.attack}`);
        target.health = target.health - this.attack;

        if (target.health <= 0){
            console.log(`${target.name} fainted`)
        }
    }
}

let pokemon01 = new Pokemon("Pikachu", 12);
let pokemon02 = new Pokemon("Geodude", 8);
let pokemon03 = new Pokemon("Mewtwo", 100);

console.log(pokemon01);
console.log(pokemon02);
pokemon02.tackle(pokemon01);
console.log(pokemon03);
pokemon03.tackle(pokemon02);
console.log(pokemon02);